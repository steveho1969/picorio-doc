Firmware
========

.. toctree::
   :maxdepth: 2
   :caption: Contents:

   ./firmware/debug_socket
   ./firmware/sdk
   ./firmware/api

+-------------------+----------------------------------------------------+
| Projects          |     Project Description                            |
+===================+====================================================+
| `Debug socket`_   | Debug-socket is proxy running on host to interact  |
|                   | with target, the functionality of debug-socket     |
|                   | in software development.                           |
+-------------------+----------------------------------------------------+
| `ES1Y SDK`_       | ES1Y SDK v1.0 provides freertos for customers'     |
|                   | application development, what's more, there are    |
|                   | some system test demos included in rvSDK so as     |
|                   | to help the new customers get on hand quickly.     |
+-------------------+----------------------------------------------------+
| `ES1Y API`_       | Includes OS API, UART API, GPIO API                |
+-------------------+----------------------------------------------------+

.. _Debug socket: ./firmware/debug_socket.html
.. _ES1Y SDK: ./firmware/sdk.html
.. _ES1Y API: ./firmware/api.html